       IDENTIFICATION DIVISION.
       PROGRAM-ID. CobolGreeting.
       *>Program to display COBOL greeting.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  IterNUM PIC 9 VALUE 5.

       PROCEDURE DIVISION .
       BeginProgram.
           PERFORM DisplayGreeting IterNUM TIMES .
           STOP RUN .
       DisplayGreeting .
           DISPLAY "Greeting from COBOL".

       
           
      
            